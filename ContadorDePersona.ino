#include <SevSeg.h>

#include <IRremote.hpp>

SevSeg sevseg;

// MODULO IR
int RECV_PIN = A5;
IRrecv irrecv(RECV_PIN);
decode_results results;


// MODULO INS333-x
int count = 0 ; 
int countPerson = 0 ; 
byte estado = 0  ; 
byte estadoPrev = 0 ; 

byte incomingByte  ; 

byte stopINS[] = {0x11, 0xA2, 0x80, 0x01, 0xE4, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x65, 0x16};
byte startINS[] = {0x11, 0xA2, 0x80, 0x01, 0xEB, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x6C, 0x16};

byte writeTime[] = {0x11, 0xA2, 0x80, 0x01, 0x08, 0x0A, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x93, 0x16};

byte WriteDirec[] = {0x11, 0xA2, 0x80, 0x01, 0x04, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x86, 0x16};
byte readDirec[] = {0x11, 0xA2, 0x80, 0x01, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x84, 0x16};

bool flagStart = true ; 


void setup() {
  // put your setup code here, to run once:
  Serial.begin(38400);
  irrecv.enableIRIn(); // Start the receiver 

  // for Display 7seg
  byte numDigits = 3;
  byte digitPins[] = {6, 7, 8};
  byte segmentPins[] = {9, 11, A2, A0, A1, 10, 12};
 // byte digitPins[] = {10, 9, 8};
  //byte segmentPins[] = {7, 5, A2, A1, A0, 6, A4};
  bool resistorsOnSegments = false; // 'false' means resistors are on digit pins
  byte hardwareConfig = COMMON_ANODE; // See README.md for options
  bool updateWithDelays = false; // Default 'false' is Recommended
  bool leadingZeros = false; // Use 'true' if you'd like to keep the leading zeros
  bool disableDecPoint = true; // Use 'true' if your decimal point doesn't exist or isn't connected. Then, you only need to specify 7 segmentPins[]

  sevseg.begin(hardwareConfig, numDigits, digitPins, segmentPins, resistorsOnSegments,
  updateWithDelays, leadingZeros, disableDecPoint); 

  sevseg.setNumber(countPerson,0);

  pinMode(A4 , OUTPUT);
  pinMode(A3 , OUTPUT) ;
  pinMode(13 , OUTPUT) ;
  digitalWrite(A4 , HIGH);
  digitalWrite(A3 , HIGH) ;
  digitalWrite(13 , HIGH) ;
  

}

void loop() {
   
  sevseg.refreshDisplay();
  sevseg.setBrightness(90);
  // put your main code here, to run repeatedly:
  if(Serial.available() and flagStart){
        //Serial.print(Serial.read(), HEX);
        //Serial.print("\n") ;
        incomingByte = Serial.read() ;
        
        if(incomingByte==162){
            count = 0 ;
            estadoPrev =  estado ;
            
                
          }
        count = count +1 ; 

        if(count==6){
            estado = incomingByte ; 
            Serial.print(estado) ;
            Serial.print("\n") ;
          }

        if(estado==3 and estadoPrev==1){
            countPerson = countPerson + 1 ; 
            //setea el numero 
            sevseg.setNumber(countPerson,0);

            Serial.print("\n") ;
            Serial.print(countPerson , DEC) ;
            Serial.print("\n") ; 
            estado = 0 ;  
    }

         
  }

  
  
  
  if (irrecv.decode(&results))
    {
     irrecv.resume(); // Receive the next value
    }
  
  if(results.value==0xFFA25D){
          Serial.print("boton stop");
          Serial.write(stopINS, sizeof(stopINS));
          flagStart = false ; 
          
           
      }

    if(results.value==0xFF629D){
         Serial.print("boton star");
          Serial.write(startINS, sizeof(startINS));
          flagStart = true ;
          
      }

    if(results.value==0xFFE21D){
          Serial.print("boton write");
          Serial.write(WriteDirec, sizeof(WriteDirec));

          
      }
      if(results.value==0xFF22DD){
          Serial.print("readDirec");
          Serial.write(readDirec, sizeof(readDirec));

          
      }

      if(results.value==0xFF02FD){
          Serial.print("writeTime");
          Serial.write(writeTime, sizeof(writeTime));

          
      }

      if(results.value==0xFF9867){
          Serial.print("boton reset");
          countPerson = 0 ; 
          sevseg.setNumber(countPerson,0);
          
      }
 

}
